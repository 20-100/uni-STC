#ifndef _STC8G1Kxx_SOP8_H
#define _STC8G1Kxx_SOP8_H

#define MCU_FAMILY 8
#define MCU_SERIES 'G'
#define MCU_PINS 8
#define MCU_MAX_FREQ_MHZ 36
#define CHIPID_IN_CODE
#define MCU_HAS_MDU
#define NB_TIMERS 2
#define TIMER_HAS_T1

#include <uni-STC/stcmcu.h>

#endif // _STC8G1Kxx_SOP8_H
