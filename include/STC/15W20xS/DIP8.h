#ifndef _STC15W20XS_DIP8_H
#define _STC15W20XS_DIP8_H

#define MCU_FAMILY 15
#define MCU_SERIES 'W'
#define MCU_PINS 8
#define MCU_MAX_FREQ_MHZ 35
#define MCU_HAS_COMPARATOR
#define MCU_HAS_NO_SPI
#define NB_TIMERS 2
#define PCA_CHANNELS 0
#define ADC_CHANNELS 0
#define GPIO_NO_P34
#define GPIO_NO_P35

#include <uni-STC/stcmcu.h>

#endif // _STC15W20XS_DIP8_H
