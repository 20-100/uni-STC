/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "glow-pca.h"
#include <timer-hal.h>
#include <pca-hal.h>

#define PCA_COUNTER_VALUE 255U

// Luminance as per CIELAB scaled to 256 (8-bit PWM)
static const uint8_t PCA_GLOW_GRADIENT[] = {
	1, 3, 5, 7, 10, 15, 20, 27, 34, 44, 54, 
	67, 81, 97, 114, 134, 157, 181, 208, 237,
};

#define PCA_GLOW_STEPS (sizeof(PCA_GLOW_GRADIENT) / sizeof(PCA_GLOW_GRADIENT[0]))

static int8_t pcaGlowStep = 0;
static int8_t pcaGlowIncrement = 1;

#ifndef PCA_GLOW_ENABLE_INTERRUPT

#pragma save
// Suppress warning "unreferenced function argument"
#pragma disable_warning 85
void pcaOnInterrupt(PCA_Channel channel, uint16_t HAL_PCA_SEGMENT pulseLength) {
}
#pragma restore

#endif

void pcaGlowUpdateDutyCycle() {
	pcaSetDutyCycle(PCA_GLOW_CHANNEL, PCA_COUNTER_VALUE - PCA_GLOW_GRADIENT[pcaGlowStep]);
	
	int8_t newStep = pcaGlowStep + pcaGlowIncrement;
	
	if (newStep < 0 || newStep >= PCA_GLOW_STEPS) {
		pcaGlowIncrement = -pcaGlowIncrement;
	}
	
	pcaGlowStep += pcaGlowIncrement;
}

void pcaGlowInitialise() {
	startTimer(
		TIMER0, 
		frequencyToSysclkDivisor(PCA_GLOW_COUNTER_FREQ), 
		DISABLE_OUTPUT, 
#ifdef PCA_GLOW_ENABLE_INTERRUPT
		ENABLE_INTERRUPT, 
#else
		DISABLE_INTERRUPT,
#endif
		FREE_RUNNING
	);
	
	pcaStartCounter(
		PCA_TIMER0, 
		FREE_RUNNING, 
		DISABLE_INTERRUPT, 
		PCA_GLOW_PIN_CONFIG
	);
	pcaConfigureOutput(
		PCA_GLOW_CHANNEL, 
		GPIO_BIDIRECTIONAL_MODE
	);
	pcaStartPwm(
		PCA_GLOW_CHANNEL, 
		MAKE_PCA_PWM_BITS(PCA_GLOW_PWM_BITS), 
		PCA_EDGE_NONE, 
		PCA_COUNTER_VALUE - PCA_GLOW_GRADIENT[0]
	);
}
